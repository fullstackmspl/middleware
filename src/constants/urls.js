
export const BASE_URL = 'http://93.188.167.68:6100/api';
export const LOGIN_URL = '/users/login';
export const GET_USER_URL = "/users/getById/";
export const GET_UserGroups_URL = "/users/groups/";
export const UPDATE_USER_URL = "/users/update/";
export const REGISTER_USER_URL = "/users/register";
export const ResetPassword_URL = '/users/resetPassword';
export const AddAddress_URL = '/users/addAddress';
export const AddressList_URL = '/users/addressList/';
export const AddressUpdate_URL = '/users/addressUpdate/'
export const ProductList_URL = '/products/list?';
export const GetProductPrice_URL = '/products/price';
export const ProductSearch_URL = '/products/search?';
export const ProductCategories_URL = '/products/subCategories';
export const UploadProfilePic_URL = '/users/uploadProfilePic';
export const PlaceOrder_URL = '/orders/placeOrder';
export const OrderList_URL = '/orders/list?';



