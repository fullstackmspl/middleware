/**
 * Home 3 Page Banner
 */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class HomeBanner3 extends Component {

   render() {
    return (
        <div className="row">
            <div className="col-md-6">
            <div className="Ecommerce_banner_wrapper ">
                <Link className="pgs_banner-link" to="/shop">
                <div className="Ecommerce_banner Ecommerce_banner-style-style-1 Ecommerce_banner-effect-none ciya-banner-1"> <img className="Ecommerce_banner-image img-fluid inline" alt="Banner" src={require(`../../assets/images/categories/modern/women.jpg`)}  />
                    <div className="Ecommerce_banner-content Ecommerce_banner-content-hcenter Ecommerce_banner-content-vbottom">
                    <div className="Ecommerce_banner-content-wrapper">
                        <div className="Ecommerce_banner-content-inner-wrapper">
                        <div className="Ecommerce_banner-text-wrap Ecommerce_banner-text-wrap-1 Ecommerce_banner-text-bg_color hidden-lg hidden-md hidden-sm hidden-xs">
                            <div className="Ecommerce_banner-text tag-text"> Women's collections</div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </Link>
            </div>
            </div>
            <div className="col-md-6 mt-4 mt-md-0">
            <div className="Ecommerce_banner_wrapper ">
                <Link className="pgs_banner-link" to="/shop">
                <div className="Ecommerce_banner Ecommerce_banner-style-style-1 Ecommerce_banner-effect-none ciya-banner-2"> <img className="Ecommerce_banner-image img-fluid inline" alt="Banner" src={require(`../../assets/images/categories/modern/men.jpg`)}  />
                    <div className="Ecommerce_banner-content Ecommerce_banner-content-hcenter Ecommerce_banner-content-vbottom">
                    <div className="Ecommerce_banner-content-wrapper">
                        <div className="Ecommerce_banner-content-inner-wrapper">
                        <div className="Ecommerce_banner-text-wrap Ecommerce_banner-text-wrap-1 Ecommerce_banner-text-bg_color">
                            <div className="Ecommerce_banner-text tag-text"> Men's collections</div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </Link>
            </div>
            </div>
         </div>
      )
}
}

export default HomeBanner3;

