
/**
 * Home 3 Sale Banner
 */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class HomeSaleBanner extends Component {

   render() {
    return (
        <div className="col-sm-12">
            <div className="Ecommerce_banner_wrapper">
                <div className="Ecommerce_banner Ecommerce_banner-style-style-1 Ecommerce_banner-effect-none ciya-banner-3">
                    <img className="Ecommerce_banner-image img-fluid inline" alt="Banner" src={require(`../../assets/images/categories/modern/sub-banner.jpg`)}  />
                <div className="Ecommerce_banner-content p-4 p-sm-5 Ecommerce_banner-content-hleft Ecommerce_banner-content-vmiddle ">
                    <div className="Ecommerce_banner-content-wrapper">
                    <div className="Ecommerce_banner-content-inner-wrapper">
                        <div className="Ecommerce_banner-text-wrap Ecommerce_banner-text-wrap-1 Ecommerce_banner-text-bg_color hidden-lg hidden-md hidden-sm hidden-xs">
                        <div className="Ecommerce_banner-text tag-text"> only 24 hours</div>
                        </div>
                        <div className="Ecommerce_banner-text-wrap Ecommerce_banner-text-wrap-2 hidden-lg hidden-md hidden-sm hidden-xs">
                        <div className="Ecommerce_banner-text"> Up To 50% Off</div>
                        </div>
                        <div className="Ecommerce_banner-text-wrap Ecommerce_banner-text-wrap-3 hidden-lg hidden-md hidden-sm hidden-xs">
                        <div className="Ecommerce_banner-text"> Final Sale Items</div>
                        </div>
                        <div className="Ecommerce_banner-btn-wrap Ecommerce_banner-btn-style-link Ecommerce_banner-btn-shape-square">
                        <Link to="/shop" className="Ecommerce_banner-btn inline_hover">Shop now</Link></div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
      )
}
}

export default HomeSaleBanner;

