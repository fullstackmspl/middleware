import React, { useState } from "react";
import {
  CardNumberElement,
  CardExpiryElement,
  CardCvcElement,
  useStripe,
  useElements
} from "@stripe/react-stripe-js";

const style = {
  iconStyle: "solid",
  base: {
    color: '#303238',
    fontSize: '16px',
    fontFamily: '"Open Sans", sans-serif',
    fontSmoothing: 'antialiased',
    '::placeholder': {
      color: '#CFD7DF',
    },
  },
  invalid: {
    color: '#e5424d',
    ':focus': {
      color: '#303238',
    },
  },
};

export default function CheckoutForm({ onResult }) {
  const [error, setError] = useState(null);
  const stripe = useStripe();
  const elements = useElements();

  const handleChange = event => {
    if (event.error) {
      setError(event.error.message);
    } else {
      setError(null);
    }
  };

  const handleSubmit = async event => {
    event.preventDefault();

    if (!stripe || !elements) {
      return;
    }

    const card = elements.getElement(CardNumberElement);
    const { error, token } = await stripe.createToken(card);

    if (error) {
      console.log("[error]", error);
      setError(error.message);
    } else {
      console.log("[token]", token);
      setError(null);
    }

    onResult({ error, token });
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Card number
        <CardNumberElement options={{style}} onChange={handleChange} />
      </label>
      <label>
        Expiration date
        <CardExpiryElement options={{style}} onChange={handleChange} />
      </label>
      <label>
        CVC
        <CardCvcElement options={{style}} onChange={handleChange} />
      </label>
      <div>{error}</div>
      <button type="submit" disabled={!stripe || !elements}>
        Pay
      </button>
    </form>
  );
}
