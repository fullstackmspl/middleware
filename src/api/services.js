// import axios from "axios";
import * as Url from '../constants/urls'
var base64 = require('base-64');

export const get = async (url) => {
    var headers = new Headers();
    headers.append(
        'Authorization',
        'Basic VVNfTlJXNjNRUEtKLUhTSVFHQVJGQTplc290UWNNY1Q5YTk2MHd1QXpwbHhR',
        );  
        
        const completeUrl = url
        const response = await fetch(completeUrl, {
            // mode: 'cors',
            method: 'GET',
            headers
        })
        let res = await response.json();
        if (res !== null) {
            if (res !== null && Object.keys(res).length !== 0) {
      
                    console.log('1res', res)
                    return res;
                
            }
            console.log('2res', res)
        }
    };
// export const upLoad = async (url, token, body) => {
//     var headers
//     const tok = 'US_NRW63QPKJ-HSIQGARFA:esotQcMcT9a960wuAzplxQ';
//     const hash = base64.encode(tok);
//     const basic = 'Basic ' + hash;

//     // if (hash == '' || hash == null || hash == undefined) {
//     headers = {
//         'Content-Type': 'multipart/form-data',
//         'Accept': 'application/json',
//         "Authorization": basic
//     }
//     // }
//     // else {
//     // headers = {
//     //     'Content-Type': 'multipart/form-data',
//     //     'Accept': 'application/json',
//     //     "x-access-token": basic
//     // }
//     // }

//     const completeUrl = Url.BASE_URL + url
//     console.log('completeUrl', completeUrl)
//     try {

//         const response = await fetch(completeUrl, {
//             method: 'POST',
//             headers,
//             body: body
//         });

//         let res = await response.json();

//         if (res !== null) {
//             if (res !== null && Object.keys(res).length !== 0) {
//                 if (res.statusCode === 200) {
//                     console.log('res', res)
//                     return res;
//                 }
//             }
//             console.log('res', res)
//             //  Alert.alert('', res.error)
//         }
//     } catch (err) {
//         //  Alert.alert('', " Somthing Went Wrong")
//         console.log('err', err.message);

//     }

// };

export const post = async (url, token, body) => {
    var headers
    // const tok = 'US_NRW63QPKJ-HSIQGARFA:esotQcMcT9a960wuAzplxQ';
    // const hash = base64.encode(tok);
    // const basic = 'Basic ' + hash;
    headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        // "Authorization": basic
    }
    let data = JSON.stringify(body)

    const completeUrl = Url.BASE_URL + url

    console.log('headers', headers)
    const response = await fetch(completeUrl, {
        method: 'POST',
        headers,
        body: data
    });

    let res = await response.json();

    if (res !== null) {
        if (res !== null && Object.keys(res).length !== 0) {
            if (res.statusCode === 200) {
                console.log('res', res)
                return res;
            }
        }
        throw new Error(res.error)
    }
};

export const put = async (url, token, body) => {
    var headers
    const tok = 'US_NRW63QPKJ-HSIQGARFA:esotQcMcT9a960wuAzplxQ';
    const hash = base64.encode(tok);
    const basic = 'Basic ' + hash;
    // if (token == '' || token == null || token == undefined) {
    headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Authorization": basic
    }
    // }
    // else {
    //     headers = {
    //         Accept: "application/json",
    //         "Content-Type": "application/json",
    //         "x-access-token": token
    //     }
    // }
    const completeUrl = Url.BASE_URL + url
    console.log('completeUrl', completeUrl)
    let data = JSON.stringify(body)
    try {
        const response = await fetch(completeUrl, {
            method: 'PUT',
            headers,
            body: data
        });

        let res = await response.json();

        if (res !== null) {
            if (res !== null && Object.keys(res).length !== 0) {
                if (res.statusCode === 200) {
                    console.log('res', res)
                    return res;
                }
            }
            console.log('res', res)
            //  Alert.alert('', res.error)
        }
    } catch (err) {

        console.log('err', err.message);

    }
}

// const axios = require('axios');

// async function makeRequest() {

//     const config = {
//         method: 'get',
//         url: 'http://webcode.me',
//         headers: { 'User-Agent': 'Console app' }
//     }

//     let res = await axios(config)

//     console.log(res.request._header);
// }

// makeRequest();